export function isNullOrUndefined(val: any): boolean {
  if (val === undefined ) {
    return true;
  }

  if (val === null) {
    return true;
  }

  return false;
}
